package com.esaturnus.planning.appointment.messaging.kafka;

import com.esaturnus.planning.appointment.AppointmentCancelledEvent;
import com.esaturnus.planning.appointment.AppointmentRescheduledEvent;
import com.esaturnus.planning.appointment.AppointmentScheduledEvent;
import com.esaturnus.planning.appointment.messaging.kafka.model.AppointmentScheduledKafkaEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.event.TransactionalEventListener;
import org.springframework.util.MimeTypeUtils;

@Slf4j
@Service
@RequiredArgsConstructor
public class KafkaPublisher {

    private final PlanningStreams planningStreams;

    @TransactionalEventListener
    public void publish(AppointmentScheduledEvent event) {
        log.info("Publishing AppointmentScheduledEvent: {}", event);

        AppointmentScheduledKafkaEvent e = new AppointmentScheduledKafkaEvent(event.getPatientName());

        MessageChannel messageChannel = planningStreams.outboundEvents();
        messageChannel.send(MessageBuilder
                .withPayload(e)
                .setHeader(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON)
                .build());

    }

    @TransactionalEventListener
    public void publish(AppointmentRescheduledEvent event) {
        log.info("Publishing AppointmentRescheduledEvent: {}", event);
    }

    @TransactionalEventListener
    public void publish(AppointmentCancelledEvent event) {
        log.info("Publishing AppointmentCancelledEvent: {}", event);
    }
}
