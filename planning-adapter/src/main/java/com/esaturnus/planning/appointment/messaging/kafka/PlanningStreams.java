package com.esaturnus.planning.appointment.messaging.kafka;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface PlanningStreams {
    String OUTPUT = "planning-events";

    @Output(OUTPUT)
    MessageChannel outboundEvents();
}