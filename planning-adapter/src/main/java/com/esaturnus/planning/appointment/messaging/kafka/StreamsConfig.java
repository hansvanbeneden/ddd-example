package com.esaturnus.planning.appointment.messaging.kafka;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBinding(PlanningStreams.class)
public class StreamsConfig {
}