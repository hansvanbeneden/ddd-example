package com.esaturnus.planning.appointment.messaging.kafka.model;

import com.esaturnus.planning.appointment.AppointmentId;
import lombok.Value;

import java.time.LocalDateTime;

@Value
public class AppointmentScheduledKafkaEvent {
//    private AppointmentId appointmentId;
    private String patientName;
//    private LocalDateTime localDateTime;
}
