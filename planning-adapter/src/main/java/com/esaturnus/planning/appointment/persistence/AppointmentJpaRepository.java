package com.esaturnus.planning.appointment.persistence;

import com.esaturnus.planning.appointment.Appointment;
import com.esaturnus.planning.appointment.AppointmentRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

interface AppointmentJpaRepository extends CrudRepository<Appointment, Long>, AppointmentRepository {

}
