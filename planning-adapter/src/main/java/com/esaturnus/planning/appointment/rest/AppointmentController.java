package com.esaturnus.planning.appointment.rest;

import com.esaturnus.planning.appointment.Appointment;
import com.esaturnus.planning.appointment.rest.model.AppointmentDTO;
import com.esaturnus.planning.appointment.service.AppointmentApplicationService;
import lombok.RequiredArgsConstructor;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequiredArgsConstructor
public class AppointmentController {

    private final AppointmentApplicationService appointmentService;

    @GetMapping
    public List<AppointmentDTO> findAllAppointments() {
        Iterable<Appointment> all = appointmentService.findAllAppointments();
        return StreamSupport.stream(all.spliterator(), false)
                .map(AppointmentDTO::new)
                .collect(Collectors.toList());
    }

    @PostMapping
    public AppointmentDTO scheduleAppointment(@RequestBody AppointmentDTO dto) {
        Assert.notNull(dto, "The appointmentDto cannot be null.");
        Assert.notNull(dto.getDateTime(), "The dateTime cannot be null.");
        Assert.notNull(dto.getPatientName(), "The patient name cannot be null.");
        Assert.isNull(dto.getAppointmentId(), "The appointmentId cannot have a value, it will be generated.");

        Appointment appointment = appointmentService.scheduleAppointment(dto.getPatientName(), dto.getDateTime());
        return new AppointmentDTO(appointment);
    }
}
