package com.esaturnus.planning.appointment.rest.model;

import com.esaturnus.planning.appointment.Appointment;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class AppointmentDTO {
    private String appointmentId;
    private final String patientName;
    private final LocalDateTime dateTime;

    public AppointmentDTO(Appointment appointment) {
        appointmentId = appointment.appointmentId().toString();
        patientName = appointment.patientName();
        dateTime = appointment.dateTime();
    }
}
