package com.esaturnus.planning.appointment.service;

import com.esaturnus.planning.appointment.Appointment;
import com.esaturnus.planning.appointment.AppointmentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
@Transactional
@RequiredArgsConstructor
public class AppointmentApplicationService {

    private final AppointmentRepository appointmentRepository;

    public Appointment scheduleAppointment(String patientName, LocalDateTime dateTime) {
        return appointmentRepository.save(new Appointment(patientName, dateTime));
    }

    public Iterable<Appointment> findAllAppointments() {
        return appointmentRepository.findAll();
    }
}
