package com.esaturnus.planning.appointment;

import org.springframework.data.domain.AbstractAggregateRoot;
import org.springframework.util.Assert;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.UUID;

@Entity
public class Appointment extends AbstractAggregateRoot<Appointment> {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Embedded
    private AppointmentId appointmentId;
    private String patientName;
    private LocalDateTime dateTime; //maybe create a value object with Date and Time?

    private Appointment() { // hibernate needs a default constructor
    }

    public Appointment(String patientName, LocalDateTime dateTime) {
        Assert.notNull(patientName, "PatientName cannot be null.");
        Assert.notNull(dateTime, "DateTime cannot be null.");

        appointmentId = new AppointmentId(UUID.randomUUID().toString());
        this.patientName = patientName;
        this.dateTime = dateTime;
        registerEvent(new AppointmentScheduledEvent(appointmentId, patientName, dateTime));
    }

    public void reschedule(LocalDateTime newDateTime) {
        Assert.notNull(newDateTime, "newDateTime cannot be null.");

        LocalDateTime oldDateTime = dateTime;
        dateTime = newDateTime;
        registerEvent(new AppointmentRescheduledEvent(appointmentId, oldDateTime, newDateTime));
    }

    public void cancel() {
        registerEvent(new AppointmentCancelledEvent(appointmentId));
    }

    public AppointmentId appointmentId() {
        return appointmentId;
    }

    public String patientName() {
        return patientName;
    }

    public LocalDateTime dateTime() {
        return dateTime;
    }

    // utility for asserting the domain events in tests
    Collection<Object> listDomainEvents() {
        return domainEvents();
    }

}
