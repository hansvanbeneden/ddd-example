package com.esaturnus.planning.appointment;

import lombok.Value;

@Value
public class AppointmentCancelledEvent {
    private AppointmentId appointmentId;
}
