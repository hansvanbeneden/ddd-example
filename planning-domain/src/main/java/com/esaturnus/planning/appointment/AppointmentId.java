package com.esaturnus.planning.appointment;

import javax.persistence.Embeddable;

@Embeddable
public class AppointmentId {
    private String appointmentId;

    AppointmentId(String appointmentId) {
        this.appointmentId = appointmentId;
    }

    AppointmentId() { // hibernate needs a default constructor
    }

    public String toString() {
        return appointmentId;
    }
}
