package com.esaturnus.planning.appointment;

import java.util.Optional;

public interface AppointmentRepository {
    Iterable<Appointment> findAll();

    <S extends Appointment> S save(S appointment);

    void delete(Appointment appointment);

    Optional<Appointment> findByAppointmentId(AppointmentId appointmentId);
}
