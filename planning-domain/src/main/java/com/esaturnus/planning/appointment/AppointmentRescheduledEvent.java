package com.esaturnus.planning.appointment;

import lombok.Value;

import java.time.LocalDateTime;

@Value
public class AppointmentRescheduledEvent {
    private AppointmentId appointmentId;
    private LocalDateTime oldDateTime;
    private LocalDateTime newDateTime;
}
