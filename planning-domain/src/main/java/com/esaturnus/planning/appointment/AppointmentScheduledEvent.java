package com.esaturnus.planning.appointment;

import lombok.Value;

import java.time.LocalDateTime;

@Value
public class AppointmentScheduledEvent {
    private AppointmentId appointmentId;
    private String patientName;
    private LocalDateTime localDateTime;
}
