package com.esaturnus.planning.appointment;

import org.junit.Test;

import java.time.LocalDateTime;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class AppointmentTest {

    @Test
    public void schedule() throws Exception {
        Appointment appointment = new Appointment("hans", LocalDateTime.MAX);

        Collection<Object> events = appointment.listDomainEvents();
        AppointmentScheduledEvent expected = new AppointmentScheduledEvent(new AppointmentId(), "hans", LocalDateTime.MAX);
        assertTrue(events.stream().allMatch(event -> equalsIgnoringAppointmentId(event, expected)));
    }

    @Test
    public void reschedule() throws Exception {
        Appointment appointment = new Appointment("hans", LocalDateTime.MAX);
        appointment.reschedule(LocalDateTime.MIN);

        Collection<Object> events = appointment.listDomainEvents();
        AppointmentRescheduledEvent expected = new AppointmentRescheduledEvent(appointment.appointmentId(), LocalDateTime.MAX, LocalDateTime.MIN);
        assertThat(events, hasItem(expected));
    }

    @Test
    public void cancel() throws Exception {
        Appointment appointment = new Appointment("hans", LocalDateTime.MAX);
        appointment.cancel();

        Collection<Object> events = appointment.listDomainEvents();
        AppointmentCancelledEvent expected = new AppointmentCancelledEvent(appointment.appointmentId());
        assertThat(events, hasItem(expected));
    }

    private boolean equalsIgnoringAppointmentId(Object actual, AppointmentScheduledEvent expected) {
        assertNotNull("actual is null.", actual);
        assertNotNull("expected is null.", expected);

        return actual instanceof AppointmentScheduledEvent &&
                ((AppointmentScheduledEvent) actual).getPatientName().equals(expected.getPatientName()) &&
                ((AppointmentScheduledEvent) actual).getLocalDateTime().equals(expected.getLocalDateTime());
    }

}