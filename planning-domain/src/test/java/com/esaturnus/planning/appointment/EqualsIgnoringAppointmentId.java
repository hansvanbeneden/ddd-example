package com.esaturnus.planning.appointment;

import lombok.RequiredArgsConstructor;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

@RequiredArgsConstructor
    class EqualsIgnoringAppointmentId extends TypeSafeMatcher<Appointment> {

        private final Appointment appointment;

        @Override
        protected boolean matchesSafely(Appointment item) {
            return appointment.patientName().equals(item.patientName()) &&
                    appointment.dateTime().equals(item.dateTime());
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("equal to " + appointment);
        }

        public static Matcher<Appointment> divisibleBy(Appointment divider) {
            return new EqualsIgnoringAppointmentId(divider);
        }
    }